import {combineReducers} from 'redux'

import nav from './navReducer'
import contact from './contactReducer'
import hometexts from './homeTextsReducer'
import gal1 from './galleryReducer'
 
const root = combineReducers({nav,contact,hometexts,gal1})

export default root