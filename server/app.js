const express = require("express");
const app = express();
const model = require('./lib/model')
var bodyParser = require('body-parser');
const { findAll } = require("./lib/model");
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('images'))
//   const uniqid=require("uniqid")
Html5Entities = require('html-entities').Html5Entities
 
const entities = new Html5Entities();
 


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
 
app.get('/gallery',(req,res)=>{
    model.getAll('events')
         .then(r=>{
             res.send(r)
         })
})

app.post('/filter',(req,res)=>{
    // return console.log(req.body);
    if(req.body.years==='All'){
        model.getAll('events')
         .then(r=>{
             res.send(r)
         })
    }else{
        model.findAll('year','years',req.body)
        .then(r=>{
            model.findAll('events','year_id',{id:r[0].id})
            .then(rr=>{
               return res.send(rr);
            })
        })
    }   
})

app.post('/get-event',(req,res)=>{
    model.findAll('events','id',{id:req.body.id})
         .then(r=>{
             model.findAll('gallery','event_id',{id:req.body.id})
                  .then(rr=>{
                      r=r[0]
                    return res.send({info:r,imgs:rr});
                  })
             
         })
})



console.log("server started")

  const PORT = process.env.PORT || 5000  
app.listen(PORT,()=>{
    console.log('server is started');
})