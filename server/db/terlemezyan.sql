/*
 Navicat Premium Data Transfer

 Source Server         : VahanLesson1
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : terlemezyan

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 20/01/2021 13:28:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `year_id`(`year_id`) USING BTREE,
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of events
-- ----------------------------
INSERT INTO `events` VALUES (1, 'Սերունդներ', 'gen1', 'Երևանի Փ. Թերլեմեզյանի անվան գեղարվեստի պետական քոլեջի 95-ամյակի առիթով հայ գեղանկարչի\r\nհամանուն կրթօջախում բացվեց ազգային ազատագրական պայքարի մարտիկ, Վասպուրականի\r\nազատամարտի մասնակից Փանոս Թերլեմեզյանի կիսանդրին: Կիսանդրու հեղինակն է քանդակագործ Իսակ\r\nԱպրեյանը: ՀՀ Առաջին Տիկին Ռիտա Սարգսյանի հովանավորությամբ իր դռները մեր առջև բացեց\r\nվերանորոգված «Թերլեմեզյան ցուցասրահը»: Դիզայներ` Նարեկ Գասպարյան: Բացվեց «ՍԵՐՈւՆԴՆԵՐ»\r\nխորագրով ցուցահանդեսը, որտեղ ներկայացված են քոլեջի դասախոսների և նրանց աշակերտած\r\nշրջանավարտների շուրջ 70 ստեղծագործություն:', 'Ցուցահանդեսի կազմակերպիչ` արվեստաբան Ելենա Մովսեսյան:', '10.03.2017', 1);
INSERT INTO `events` VALUES (2, 'Երեք պատմություն', 'gen2', '«Երեք պատմություն» ծրագրի շրջանակներում ս/թ նոյեմբերի 18-ին կայացավ ցուցահանդես, որն\r\nընդգրկում է քոլեջի սաների շուրջ 120 գեղանկարչական ու գրաֆիկական թեմատիկ աշխատանք, ինչպես\r\nնաև քանդակագործության և խեցեղենի յուրատիպ նմուշներ: Երկու մասից բաղկացած ծրագրի առաջին\r\nմասն ընդգրկել է երեք հանդիպում-քննարկում արդի հայ գրողներ Գրիգի, Արամ Պաչյանի և Հրաչյա\r\nՍարիբեկյանի հետ:Հանդիպումների ընթացքում հեղինակներն իրենց ընտրությամբ ընթերցել են մեկական\r\nպատմվածք, որոնց մոտիվներով էլ ուսանողները ստեղծագործել են: Ծրագրի երկրորդ մասը ցուցահանդեսն\r\nէ, որը ներկայացնում է միջմշակութային կապեր ձևավորող ստեղծագործական համագործակցության\r\nարդյունքը։', 'Ծրագրի համադրող՝ Ելենա Մովսեսյան', '18.11.2017', 1);
INSERT INTO `events` VALUES (3, 'Էլիբեկյան', 'gen3', 'Թերլեմեզյան ցուցասրահում տեղի ունեցավ Վարպետաց դաս՝ նվիրված իտալացի կոմպոզիտոր Ռուջերո\r\nԼեոնկավալոյի «Պայացներ» օպերայի առաջին բեմադրության 125-ամյակին (օպերայի առաջին\r\nբեմադրությունը եղել է Միլանում 1892 թ. մայիսի 21-ին Արթուրո Տոսկանինիի դիրիժորությամբ):\r\nՔոլեջի ուսանողների համար Վարպետաց դասը վարեց ՀՀ ժողովրդական նկարիչ Ռոբերտ Էլիբեկյանը: Մեր\r\nպատվավոր հյուրն էր Արտավազդ Փելեշյանը:', NULL, '20.05.2017', 1);
INSERT INTO `events` VALUES (4, 'Մեր հյուրն էր բանաստեղծ Էդուարդ Հարենցը', 'gen4', 'Մեր հյուրն էր դերասանուհի,նկարչուհի Նարե Հայկազյանը', NULL, '27․09․2017', 1);
INSERT INTO `events` VALUES (5, 'Դինա Մայրեդը', 'gen5', 'Մեր հյուրն էր Հորդանանի արքայադուստր Դինա Մայրեդը', NULL, '29.04.2017', 1);
INSERT INTO `events` VALUES (6, 'Ցուցահանդես', 'gen6', '2017-2018 քննական լավագույն աշխատանքների ցուցահանդես', NULL, '28.09.2018', 2);
INSERT INTO `events` VALUES (7, 'Վիկտոր Էհիխամենոր', 'gen7', 'ICAE2018. «Ժամանակակից արվեստի միջազգային ցուցահանդես. Հայաստան, 2018» նախագծի\r\nշրջանակում քոլեջում տեղի ունեցավ երկօրյա աշխատաժողով նիգերիացի արվեստագետ Վիկտոր\r\nԷհիխամենորի հետ:', NULL, '29.09.2018', 2);
INSERT INTO `events` VALUES (8, 'Երևանյան էսքիզներ', 'gen8', 'Սերունդներ հաշվելիս գիտնականները որպես հիմք վերցնում են 25 տարին: Յուրաքանչյուր սերունդ\r\nունենում է իր գաղափարներն ու գեղագիտական ընկալումները: Անցյալ սերնդի փայփայած արժեքները\r\nվերանայվում են, վերարժևորվում:', 'Թերլեմեզյանցիները փորձել են տեսնել 2800-ամյա մեր մայրաքաղաքը Հայոց պետականության 100\r\nտարվա ժամանակահատվածում:', '29.11.2018', 2);

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `event_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `event_id`(`event_id`) USING BTREE,
  CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES (1, 'gen1', 1);
INSERT INTO `gallery` VALUES (2, 'gen2', 1);
INSERT INTO `gallery` VALUES (3, 'gen3', 1);
INSERT INTO `gallery` VALUES (4, 'gen4', 1);
INSERT INTO `gallery` VALUES (5, 'gen5', 1);
INSERT INTO `gallery` VALUES (6, 'gen6', 1);
INSERT INTO `gallery` VALUES (7, 'gen7', 1);
INSERT INTO `gallery` VALUES (8, 'gen8', 1);
INSERT INTO `gallery` VALUES (9, 'gen9', 1);
INSERT INTO `gallery` VALUES (10, 'gen10', 1);
INSERT INTO `gallery` VALUES (11, 'gen11', 1);
INSERT INTO `gallery` VALUES (12, 'gen12', 1);
INSERT INTO `gallery` VALUES (13, 'gen13', 1);
INSERT INTO `gallery` VALUES (14, 'gen14', 1);
INSERT INTO `gallery` VALUES (15, 'gen15', 1);
INSERT INTO `gallery` VALUES (16, 'gen16', 1);
INSERT INTO `gallery` VALUES (17, 'gen17', 1);
INSERT INTO `gallery` VALUES (18, 'gen18', 1);
INSERT INTO `gallery` VALUES (19, 'gen19', 1);
INSERT INTO `gallery` VALUES (20, 'th1', 2);
INSERT INTO `gallery` VALUES (21, 'th2', 2);
INSERT INTO `gallery` VALUES (22, 'th3', 2);
INSERT INTO `gallery` VALUES (23, 'th4', 2);
INSERT INTO `gallery` VALUES (24, 'th5', 2);
INSERT INTO `gallery` VALUES (25, 'th6', 2);
INSERT INTO `gallery` VALUES (26, 'th7', 2);
INSERT INTO `gallery` VALUES (27, 'th8', 2);
INSERT INTO `gallery` VALUES (28, 'th9', 2);
INSERT INTO `gallery` VALUES (29, 'th10', 2);
INSERT INTO `gallery` VALUES (30, 'th11', 2);
INSERT INTO `gallery` VALUES (31, 'th12', 2);
INSERT INTO `gallery` VALUES (32, 'th13', 2);
INSERT INTO `gallery` VALUES (33, 'th14', 2);
INSERT INTO `gallery` VALUES (34, 'th15', 2);
INSERT INTO `gallery` VALUES (35, 'th16', 2);
INSERT INTO `gallery` VALUES (36, 'th17', 2);
INSERT INTO `gallery` VALUES (37, 'th18', 2);
INSERT INTO `gallery` VALUES (38, 'th19', 2);
INSERT INTO `gallery` VALUES (39, 'th20', 2);
INSERT INTO `gallery` VALUES (40, 'th21', 2);
INSERT INTO `gallery` VALUES (41, 'th22', 2);
INSERT INTO `gallery` VALUES (42, 'th23', 2);
INSERT INTO `gallery` VALUES (43, 'th24', 2);
INSERT INTO `gallery` VALUES (44, 'th25', 2);
INSERT INTO `gallery` VALUES (45, 'th26', 2);
INSERT INTO `gallery` VALUES (46, 'th27', 2);
INSERT INTO `gallery` VALUES (47, 'th28', 2);
INSERT INTO `gallery` VALUES (48, 'th29', 2);
INSERT INTO `gallery` VALUES (49, 'th30', 2);
INSERT INTO `gallery` VALUES (50, 'th31', 2);
INSERT INTO `gallery` VALUES (51, 'th32', 2);
INSERT INTO `gallery` VALUES (52, 'th33', 2);
INSERT INTO `gallery` VALUES (54, 'mas1', NULL);
INSERT INTO `gallery` VALUES (55, 'mas2', NULL);
INSERT INTO `gallery` VALUES (56, 'mas3', NULL);
INSERT INTO `gallery` VALUES (57, 'mas4', NULL);
INSERT INTO `gallery` VALUES (58, 'mas5', NULL);
INSERT INTO `gallery` VALUES (59, 'mas6', NULL);
INSERT INTO `gallery` VALUES (60, 'mas7', NULL);
INSERT INTO `gallery` VALUES (61, 'mas8', NULL);
INSERT INTO `gallery` VALUES (62, 'mas9', NULL);
INSERT INTO `gallery` VALUES (63, 'mas10', NULL);
INSERT INTO `gallery` VALUES (64, 'mas11', NULL);
INSERT INTO `gallery` VALUES (65, 'mas12', NULL);
INSERT INTO `gallery` VALUES (66, 'mas13', NULL);
INSERT INTO `gallery` VALUES (67, 'mas14', NULL);
INSERT INTO `gallery` VALUES (68, 'mas15', NULL);
INSERT INTO `gallery` VALUES (69, 'mas16', NULL);
INSERT INTO `gallery` VALUES (70, 'mas17', NULL);
INSERT INTO `gallery` VALUES (71, 'mas18', NULL);
INSERT INTO `gallery` VALUES (72, 'mas19', NULL);
INSERT INTO `gallery` VALUES (73, 'mas20', NULL);
INSERT INTO `gallery` VALUES (74, 'mas21', NULL);
INSERT INTO `gallery` VALUES (75, 'mas22', 3);
INSERT INTO `gallery` VALUES (76, 'mas23', 3);
INSERT INTO `gallery` VALUES (77, 'mas24', 3);
INSERT INTO `gallery` VALUES (78, 'mas25', 3);
INSERT INTO `gallery` VALUES (79, 'mas26', 3);
INSERT INTO `gallery` VALUES (80, 'mas27', 3);
INSERT INTO `gallery` VALUES (81, 'mas28', 3);
INSERT INTO `gallery` VALUES (82, 'mas29', 3);
INSERT INTO `gallery` VALUES (83, 'mas30', 3);
INSERT INTO `gallery` VALUES (84, 'mas31', 3);
INSERT INTO `gallery` VALUES (85, 'mas32', 3);
INSERT INTO `gallery` VALUES (86, 'mas33', 3);
INSERT INTO `gallery` VALUES (87, 'mas34', 3);
INSERT INTO `gallery` VALUES (88, 'mas35', 3);
INSERT INTO `gallery` VALUES (89, 'mas36', 3);
INSERT INTO `gallery` VALUES (90, 'mas37', 3);
INSERT INTO `gallery` VALUES (91, 'mas38', 3);
INSERT INTO `gallery` VALUES (92, 'mas39', 3);
INSERT INTO `gallery` VALUES (93, 'mas40', 3);
INSERT INTO `gallery` VALUES (94, 'mas41', 3);
INSERT INTO `gallery` VALUES (95, 'mas42', 3);
INSERT INTO `gallery` VALUES (96, 'mas43', 3);
INSERT INTO `gallery` VALUES (97, 'mas44', 3);
INSERT INTO `gallery` VALUES (98, 'mas45', 3);
INSERT INTO `gallery` VALUES (99, 'mas46', 3);
INSERT INTO `gallery` VALUES (100, 'mas47', 3);
INSERT INTO `gallery` VALUES (101, 'mas48', 3);
INSERT INTO `gallery` VALUES (102, 'mas49', 3);
INSERT INTO `gallery` VALUES (103, 'mas50', 3);
INSERT INTO `gallery` VALUES (104, 'mas51', 3);
INSERT INTO `gallery` VALUES (105, 'mas52', 3);
INSERT INTO `gallery` VALUES (106, 'mas53', 3);
INSERT INTO `gallery` VALUES (107, 'mas54', 3);
INSERT INTO `gallery` VALUES (108, 'mas55', 3);
INSERT INTO `gallery` VALUES (109, 'mas56', 3);
INSERT INTO `gallery` VALUES (110, 'mas57', 3);

-- ----------------------------
-- Table structure for generations
-- ----------------------------
DROP TABLE IF EXISTS `generations`;
CREATE TABLE `generations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `year_id`(`year_id`) USING BTREE,
  CONSTRAINT `generations_ibfk_1` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of generations
-- ----------------------------
INSERT INTO `generations` VALUES (1, 'Սերունդներ', 'DA2B7798.jpg', 'Երևանի Փ. Թերլեմեզյանի անվան գեղարվեստի պետական քոլեջի 95-ամյակի առիթով հայ գեղանկարչի\r\nհամանուն կրթօջախում բացվեց ազգային ազատագրական պայքարի մարտիկ, Վասպուրականի\r\nազատամարտի մասնակից Փանոս Թերլեմեզյանի կիսանդրին: Կիսանդրու հեղինակն է քանդակագործ Իսակ\r\nԱպրեյանը: ՀՀ Առաջին Տիկին Ռիտա Սարգսյանի հովանավորությամբ իր դռները մեր առջև բացեց\r\nվերանորոգված «Թերլեմեզյան ցուցասրահը»: Դիզայներ` Նարեկ Գասպարյան: Բացվեց «ՍԵՐՈւՆԴՆԵՐ»\r\nխորագրով ցուցահանդեսը, որտեղ ներկայացված են քոլեջի դասախոսների և նրանց աշակերտած\r\nշրջանավարտների շուրջ 70 ստեղծագործություն:', 'Ցուցահանդեսի կազմակերպիչ` արվեստաբան Ելենա Մովսեսյան:', '10.03.2017', 1);
INSERT INTO `generations` VALUES (2, 'Երեք պատմություն', '23631976_1678617665530398_9093003882057854249_o.jpg', '«Երեք պատմություն» ծրագրի շրջանակներում ս/թ նոյեմբերի 18-ին կայացավ ցուցահանդես, որն\r\nընդգրկում է քոլեջի սաների շուրջ 120 գեղանկարչական ու գրաֆիկական թեմատիկ աշխատանք, ինչպես\r\nնաև քանդակագործության և խեցեղենի յուրատիպ նմուշներ: Երկու մասից բաղկացած ծրագրի առաջին\r\nմասն ընդգրկել է երեք հանդիպում-քննարկում արդի հայ գրողներ Գրիգի, Արամ Պաչյանի և Հրաչյա\r\nՍարիբեկյանի հետ:Հանդիպումների ընթացքում հեղինակներն իրենց ընտրությամբ ընթերցել են մեկական\r\nպատմվածք, որոնց մոտիվներով էլ ուսանողները ստեղծագործել են: Ծրագրի երկրորդ մասը ցուցահանդեսն\r\nէ, որը ներկայացնում է միջմշակութային կապեր ձևավորող ստեղծագործական համագործակցության\r\nարդյունքը։', 'Ծրագրի համադրող՝ Ելենա Մովսեսյան', '18.11.2017', 1);
INSERT INTO `generations` VALUES (3, 'Էլիբեկյան', 'DA2B8438.jpg', 'Թերլեմեզյան ցուցասրահում տեղի ունեցավ Վարպետաց դաս՝ նվիրված իտալացի կոմպոզիտոր Ռուջերո\r\nԼեոնկավալոյի «Պայացներ» օպերայի առաջին բեմադրության 125-ամյակին (օպերայի առաջին\r\nբեմադրությունը եղել է Միլանում 1892 թ. մայիսի 21-ին Արթուրո Տոսկանինիի դիրիժորությամբ):\r\nՔոլեջի ուսանողների համար Վարպետաց դասը վարեց ՀՀ ժողովրդական նկարիչ Ռոբերտ Էլիբեկյանը: Մեր\r\nպատվավոր հյուրն էր Արտավազդ Փելեշյանը:', NULL, '20.05.2017', 1);
INSERT INTO `generations` VALUES (4, 'Մեր հյուրն էր բանաստեղծ Էդուարդ Հարենցը', 'watermarked-DA2B6738.jpg', 'Մեր հյուրն էր դերասանուհի,նկարչուհի Նարե Հայկազյանը', NULL, '27․09․2017', 1);
INSERT INTO `generations` VALUES (5, 'Դինա Մայրեդը', 'DA2B0920.jpg', 'Մեր հյուրն էր Հորդանանի արքայադուստր Դինա Մայրեդը', NULL, '29.04.2017', 1);
INSERT INTO `generations` VALUES (6, 'Ցուցահանդես', '42596787_2063888903669937_7231508205178716160_n.jpg', '2017-2018 քննական լավագույն աշխատանքների ցուցահանդես', NULL, '28.09.2018', 2);
INSERT INTO `generations` VALUES (7, 'Վիկտոր Էհիխամենոր', 'IMG_1836.jpg', 'ICAE2018. «Ժամանակակից արվեստի միջազգային ցուցահանդես. Հայաստան, 2018» նախագծի\r\nշրջանակում քոլեջում տեղի ունեցավ երկօրյա աշխատաժողով նիգերիացի արվեստագետ Վիկտոր\r\nԷհիխամենորի հետ:', NULL, '29.09.2018', 2);
INSERT INTO `generations` VALUES (8, 'Երևանյան էսքիզներ', 'DA2B6792.jpg', 'Սերունդներ հաշվելիս գիտնականները որպես հիմք վերցնում են 25 տարին: Յուրաքանչյուր սերունդ\r\nունենում է իր գաղափարներն ու գեղագիտական ընկալումները: Անցյալ սերնդի փայփայած արժեքները\r\nվերանայվում են, վերարժևորվում:', 'Թերլեմեզյանցիները փորձել են տեսնել 2800-ամյա մեր մայրաքաղաքը Հայոց պետականության 100\r\nտարվա ժամանակահատվածում:', '29.11.2018', 2);

-- ----------------------------
-- Table structure for year
-- ----------------------------
DROP TABLE IF EXISTS `year`;
CREATE TABLE `year`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `years` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of year
-- ----------------------------
INSERT INTO `year` VALUES (1, 2017);
INSERT INTO `year` VALUES (2, 2018);
INSERT INTO `year` VALUES (3, 2019);
INSERT INTO `year` VALUES (4, 2020);

SET FOREIGN_KEY_CHECKS = 1;
