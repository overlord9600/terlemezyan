var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'terlemezyan'
});

connection.connect();

module.exports={
  getAll:(table)=>{
    return new Promise((resolve,reject)=>{
      connection.query(`select * from ${table}`,(err,data)=>{
        if(err)reject(err);
        resolve(data);
      })
    })
  },

  findAll:(table,col,obj)=>{
    return new Promise((resolve,reject)=>{
      let query = `SELECT * FROM ${table} where ${col}=`
      for(let key in obj){
        query+=`${obj[key]}`
      }
      connection.query(query,(err,data)=>{
        if(err) reject(err);
        resolve(data);
      })
    })
  },  
}